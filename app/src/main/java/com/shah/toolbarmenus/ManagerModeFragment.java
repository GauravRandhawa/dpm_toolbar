package com.shah.toolbarmenus;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ManagerModeFragment extends Fragment {

    View managerModeView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        managerModeView = inflater.inflate(R.layout.manager_mode, container, false);
        return managerModeView;
    }
}
