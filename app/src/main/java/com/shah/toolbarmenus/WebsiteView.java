package com.shah.toolbarmenus;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;

public class WebsiteView extends MainActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View contentView = inflater.inflate(R.layout.activity_website_view, null, false);
        drawer.addView(contentView, 0);
        WebView wv = (WebView) findViewById(R.id.webView);
        wv.loadUrl("https://www.wintec.ac.nz/");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater rightMenuInflater = getMenuInflater();
        rightMenuInflater.inflate(R.menu.right_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.visit_website){
            Intent myIntent = new Intent(this, WebsiteView.class);
            startActivity(myIntent);
        }

        else if(item.getItemId() == R.id.about){
            Intent myIntent = new Intent(this, AboutPage.class);
            startActivity(myIntent);
        }
        return true;
    }
}
